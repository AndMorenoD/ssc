from django.db import models

# Create your models here.

CUMPLE_CHOICES = (

    ('SI','SI'),
    ('NO','NO'),
   
)


CIUDAD_CHOICES = (

    ('PASTO','PASTO'),
   
   
)

class Inspeccion(models.Model):
    numero_inspeccion = models.CharField(max_length=100)
    fecha_apertura = models.CharField(max_length=100)
    fecha_cierre = models.CharField(max_length=100, blank=True, null=True)
    nombre_proyecto = models.CharField(max_length=100)
    ciudad = models.CharField(default='PASTO',  max_length=100, blank=True, null=True)
    alcance_inspeccion = models.TextField(max_length=5000)
    observaciones = models.TextField(max_length=5000, blank=True, null=True)

    def __str__(self):
        return self.nombre_proyecto

class Asistentes(models.Model):
    inspeccion = models.ForeignKey(Inspeccion, on_delete=models.CASCADE)
    empresa = models.CharField(max_length=100)
    nombre_apellido = models.CharField(max_length=100)
    cargo = models.CharField(max_length=100)
    cedula = models.CharField(max_length=100)

class Conformidad(models.Model):
    inspeccion = models.ForeignKey(Inspeccion, on_delete=models.CASCADE)
    descripcion = models.TextField(max_length=5000)
    codigo_formato = models.CharField(max_length=100)
    cierre = models.CharField(max_length=100)
    fecha_cierre = models.CharField(max_length=100)
    cumple = models.CharField(choices = CUMPLE_CHOICES,  max_length=2)