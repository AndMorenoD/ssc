from django import forms
from .models import *

class InspeccionForm(forms.ModelForm):
    class Meta:
        model = Inspeccion
        fields= '__all__'
        widgets = {
            
            'numero_inspeccion': forms.TextInput(attrs={'class':'form-control'}),
            'nombre_proyecto': forms.TextInput(attrs={'class':'form-control'}),
            'alcance_inspeccion': forms.Textarea(attrs={'class':'form-control text-justify'}),
            'observaciones': forms.Textarea(attrs={'class':'form-control text-justify'}),
            'ciudad' : forms.Select(attrs={'class':'selectpicker'}),
            'fecha_apertura' : forms.DateInput(attrs={'class':'form-control datepicker' }),
            'fecha_cierre' : forms.DateInput(attrs={'class':'form-control datepicker' }),
            
         }

class AsistenteForm(forms.ModelForm):
    class Meta:
        model = Asistentes
        fields= '__all__'
        widgets = {
            
            'empresa': forms.TextInput(attrs={'class':'form-control '}),
            'cargo': forms.TextInput(attrs={'class':'form-control '}),
            'cedula': forms.TextInput(attrs={'class':'form-control'}),
            'nombre_apellido': forms.TextInput(attrs={'class':'form-control '}),
            'inspeccion' : forms.Select(attrs={'id':'inspeccion'}),
         }

class ConformidadForm(forms.ModelForm):
    class Meta:
        model = Conformidad
        fields= '__all__'
        widgets = {
            
            'descripcion': forms.Textarea(attrs={'class':'form-control'}),
            'codigo_formato': forms.TextInput(attrs={'class':'form-control'}),
            'cierre': forms.TextInput(attrs={'class':'form-control'}),
            'fecha_cierre' : forms.DateInput(attrs={'class':'form-control datepicker' }),
            'inspeccion' : forms.Select(attrs={'id':'inspeccion'}),
            'cumple' : forms.Select(attrs={'class':'selectpicker'}),
         }