from django.shortcuts import render
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django.views.generic import ListView, TemplateView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from appInspeccion import forms
from .models import *
from django.core import serializers
from django.http import HttpResponse

# INSPECCIONES #
class InspeccionLista(LoginRequiredMixin, ListView):
    model = Inspeccion
    context_object_name = 'listaInspeccion'
    template_name = 'inspeccion/listaInspecciones.html'
    

class InspeccionCreate(LoginRequiredMixin, CreateView):
    model = Inspeccion
    form_class = forms.InspeccionForm
    template_name = 'inspeccion/crearInspeccion.html'
    success_url = reverse_lazy('Inspeccion_lista')

class InspeccionUpdate(LoginRequiredMixin,UpdateView):
    model = Inspeccion
    form_class = forms.InspeccionForm
    template_name = 'inspeccion/modificarInspeccion.html'
    success_url = reverse_lazy('Inspeccion_lista')

class InspeccionDelete(LoginRequiredMixin,DeleteView):
    model = Inspeccion
    template_name = 'inspeccion/eliminarInspeccion.html'
    success_url = reverse_lazy('Inspeccion_lista')


class InspeccionImprimir(LoginRequiredMixin,UpdateView):
    model = Inspeccion
    form_class = forms.InspeccionForm
    template_name = 'impresion/imprimirInspeccion.html'
    success_url = reverse_lazy('Inspeccion_lista')



# ASISTENTES #

class AsistenteLista(LoginRequiredMixin, ListView):
    model = Asistentes
    context_object_name = 'listaAsistentes'
    template_name = 'asistentes/listaAsistentes.html'
    

class AsistenteCreate(LoginRequiredMixin, CreateView):
    model = Asistentes
    form_class = forms.AsistenteForm
    template_name = 'asistentes/crearAsistente.html'
    success_url = reverse_lazy('Asistente_lista')

class AsistenteUpdate(LoginRequiredMixin,UpdateView):
    model = Asistentes
    form_class = forms.AsistenteForm
    template_name = 'asistentes/modificarAsistente.html'
    success_url = reverse_lazy('Asistente_lista')

class AsistenteDelete(LoginRequiredMixin,DeleteView):
    model = Asistentes
    template_name = 'asistentes/eliminarAsistente.html'
    success_url = reverse_lazy('Asistente_lista')


# CONFORMIDAD #

class ConformidadLista(LoginRequiredMixin, ListView):
    model = Conformidad
    context_object_name = 'listaConformidad'
    template_name = 'conformidad/listaConformidad.html'
    

class ConformidadCreate(LoginRequiredMixin, CreateView):
    model = Conformidad
    form_class = forms.ConformidadForm
    template_name = 'conformidad/crearConformidad.html'
    success_url = reverse_lazy('Conformidad_lista')

class ConformidadUpdate(LoginRequiredMixin,UpdateView):
    model = Conformidad
    form_class = forms.ConformidadForm
    template_name = 'conformidad/modificarConformidad.html'
    success_url = reverse_lazy('Conformidad_lista')

class ConformidadDelete(LoginRequiredMixin,DeleteView):
    model = Conformidad
    template_name = 'conformidad/eliminarConformidad.html'
    success_url = reverse_lazy('Conformidad_lista')


class ListaAjaxConformidades(TemplateView):
	def get(self, request, *args, **kwards):
		id_inspeccion = request.GET['id']
		conformidades = Conformidad.objects.filter(inspeccion__id=id_inspeccion)
		dataConformidades = serializers.serialize('json', conformidades, fields=('inspeccion','descripcion','codigo_formato','cierre','fecha_cierre','cumple'))
		return HttpResponse(dataConformidades,content_type="application/json")


class ListaAjaxAsistentes(TemplateView):
	def get(self, request, *args, **kwards):
		id_inspeccion = request.GET['id']
		asistentes = Asistentes.objects.filter(inspeccion__id=id_inspeccion)
		dataAsistentes = serializers.serialize('json', asistentes, fields=('inspeccion','empresa','nombre_apellido','cargo','cedula'))
		return HttpResponse(dataAsistentes,content_type="application/json")