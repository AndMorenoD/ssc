from django.urls import path,include
from . import views

urlpatterns = [

    #INSPECCIONES
    path('inspeccionLista/',views.InspeccionLista.as_view(),name='Inspeccion_lista'),
    path('inspeccionCrear/',views.InspeccionCreate.as_view(),name='Inspeccion_create'),
    path('inspeccionModificar/<int:pk>/',views.InspeccionUpdate.as_view(),name='Inspeccion_update'),
    path('inspeccionEliminar/<int:pk>/', views.InspeccionDelete.as_view(), name='Inspeccion_delete'),
    path('inspeccionImprimir/<int:pk>/',views.InspeccionImprimir.as_view(),name='Inspeccion_imprimir'),

    # ASISTENTES 
    path('AsistenteLista/',views.AsistenteLista.as_view(),name='Asistente_lista'),
    path('AsistenteCrear/',views.AsistenteCreate.as_view(),name='Asistente_create'),
    path('AsistenteModificar/<int:pk>/',views.AsistenteUpdate.as_view(),name='Asistente_update'),
    path('AsistenteEliminar/<int:pk>/', views.AsistenteDelete.as_view(), name='Asistente_delete'),

    # CONFORMIDAD 
    path('ConformidadLista/',views.ConformidadLista.as_view(),name='Conformidad_lista'),
    path('ConformidadCrear/',views.ConformidadCreate.as_view(),name='Conformidad_create'),
    path('ConformidadModificar/<int:pk>/',views.ConformidadUpdate.as_view(),name='Conformidad_update'),
    path('ConformidadEliminar/<int:pk>/', views.ConformidadDelete.as_view(), name='Conformidad_delete'),

    # IMPRESION
    path('DatosConformidades/',views.ListaAjaxConformidades.as_view(),name='conformidad_datos'),
    path('DatosAsistentes/',views.ListaAjaxAsistentes.as_view(),name='asistentes_datos'),

]