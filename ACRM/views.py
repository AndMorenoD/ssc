from django.shortcuts import render
from django.contrib.auth.decorators import login_required

# Create your views here.

def bienvenida(request):
    return render(request, 'bienvenida.html')

@login_required()
def base(request):
    return render(request, 'base.html')

